start: ## Inicia los contenedores
	@./bin/start || true
stop: ## Cierra y elimina los contenedores
	@./bin/stop
logs: ## muestra los contenedores del log
	@cd ./deploy/development && docker-compose logs -f --tail 200 || true
bash-web: ## Ejecuta el bash del contenedor web
	@cd ./deploy/development && docker-compose exec web bash || true
mysql: ## Accedes al mysql dentro del servidor
	@cd ./deploy/development && docker-compose exec mysqlserver bash -c "mysql -u lolamarket -p'12345678' lolamarket"
build-images: ## Contruye las imagenes si ha habido cambio en el Dockerfile
	@cd ./deploy/development && docker-compose build
ping-web-mysql: ## Comprueba que haya conexion entre el servidor web y mysql
	@cd ./deploy/development && docker-compose exec web ping mysqlserver
migrate: ## Genera la base de datos
	@cd ./deploy/development && docker-compose exec mysqlserver bash -c "mysql -u lolamarket -p'12345678' lolamarket < /root/creates.sql"
seed: ## Pobla la base de datos
	@cd ./deploy/development && docker-compose exec mysqlserver bash -c "mysql -u lolamarket -p'12345678' --default_character_set utf8 lolamarket < /root/inserts.sql"
migrate-seed: ## Genera y pobla la base de datos
	@cd ./deploy/development && docker-compose exec mysqlserver bash -c "mysql -u lolamarket -p'12345678' lolamarket < /root/creates.sql"
	@cd ./deploy/development && docker-compose exec mysqlserver bash -c "mysql -u lolamarket -p'12345678' --default_character_set utf8 lolamarket < /root/inserts.sql"
rep-productos: ## Muestra los productos agregando los valores del shopper, tienda y pedido
	@./bin/pedidoShopperSucursal
help: ## Muestra ayuda
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
