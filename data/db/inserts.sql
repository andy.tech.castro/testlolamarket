-- Usuarios
INSERT INTO usuarios(nombre,apellidos,email,telefono) 
VALUES ('Andrés', 'Castro','andres@castro.com','674608348');

INSERT INTO usuarios(nombre,apellidos,email,telefono) 
VALUES ('Esther', 'López','esther@lopez.com','673678371');

-- Direcciones
INSERT INTO direcciones(direccion,cp,ciudad,provincia,pais,idUsuario,principal,activa) 
VALUES('Calle Tragaluz 8', '28045', 'Madrid', 'Madrid','España',1,1,1);

INSERT INTO direcciones(direccion,cp,ciudad,provincia,pais,idUsuario,principal,activa) 
VALUES('Calle Bocadillos 16', '28075', 'Madrid', 'Madrid','España',1,0,1);

INSERT INTO direcciones(direccion,cp,ciudad,provincia,pais,idUsuario,principal,activa) 
VALUES('Calle Estepona 124', '28025', 'Madrid', 'Madrid','España',2,1,1);

INSERT INTO direcciones(direccion,cp,ciudad,provincia,pais,idUsuario,principal,activa) 
VALUES('Calle Carpinteros 16', '28028', 'Madrid', 'Madrid','España',2,0,1);

-- Tiendas
INSERT INTO tiendas(nombre)
VALUE('Carrefour'),
('Dia'),
('Mercadona');

-- Sucursales
INSERT INTO sucursales(idTienda,telefono,direccion,cp)
VALUE('1','634839494','Calle Alcazares 28','28045');

INSERT INTO sucursales(idTienda,telefono,direccion,cp)
VALUE('1','628234964','Calle Fuentes 48','28045');

INSERT INTO sucursales(idTienda,telefono,direccion,cp)
VALUE('1','634728164','Calle Fontaneros 69','28045');

-- Productos
INSERT INTO productos(nombre,descripcion,precio,idSucursal)
VALUE('Chocolate Negro Lindt', 'Chocolate 100% Cacao Marca Lindt', 1.50, 1);

INSERT INTO productos(nombre,descripcion,precio,idSucursal)
VALUE('Mortadela con Aceitunas', 'Mortadela con aceitunas marca Carrefour', 3.50, 1);

INSERT INTO productos(nombre,descripcion,precio,idSucursal)
VALUE('Pan de Molde', 'Pan de Molde para Tostada marca Bimbo', 2.00, 1);

INSERT INTO productos(nombre,descripcion,precio,idSucursal)
VALUE('Queso Mozzarella', 'Queso Mozzarella rallado marca Dia', 4.00, 2);

INSERT INTO productos(nombre,descripcion,precio,idSucursal)
VALUE('Sardina', 'Sardina con Picante', 1.15, 2);

INSERT INTO productos(nombre,descripcion,precio,idSucursal)
VALUE('Servilletas', 'Servilletas 200 Unidades', 3.20, 2);

-- Shoppers
INSERT INTO shoppers (nombre, apellidos)
VALUE('Santiago','Pozuelo');

INSERT INTO shoppers (nombre, apellidos)
VALUE('Hernan','Díaz');

-- Franjas Horarias
INSERT INTO franjasHorarias(franjaHoraria)
VALUE('9:00 - 11:00'),
('11:00 - 13:00'),
('13:00 - 15:00'),
('15:00 - 17:00'),
('17:00 - 19:00'),
('19:00 - 21:00');

-- Pedidos
INSERT INTO pedidos(idUsuario,idShopper,idDireccion,fechaEntrega,idFranjaHoraria,total)
VALUE(1,1,1,'2020-11-12',2,24.35);

INSERT INTO pedidos(idUsuario,idShopper,idDireccion,fechaEntrega,idFranjaHoraria,total)
VALUE(2,2,3,'2020-11-12',2,24.85);

-- Productos del Pedido
INSERT INTO pedidoProductos(idPedido,idProducto,unidades,precio)
VALUE(1,1,1,1.50),
(1,2,2,7.00),
(1,3,1,2.00),
(1,4,1,4.00),
(1,5,3,3.45),
(1,6,2,6.40);

INSERT INTO pedidoProductos(idPedido,idProducto,unidades,precio)
VALUE(2,1,2,3.00),
(2,2,1,3.50),
(2,3,3,6.00),
(2,4,2,8.00),
(2,5,1,1.15),
(2,6,1,3.20);
