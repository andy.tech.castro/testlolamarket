DROP DATABASE lolamarket;

CREATE DATABASE lolamarket CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

use lolamarket;

CREATE TABLE usuarios (
    idUsuario INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(20) NOT NULL,
    apellidos VARCHAR(20) NOT NULL,
    email VARCHAR(50) NOT NULL,
    telefono VARCHAR(10) NOT NULL,
    PRIMARY KEY (idUsuario)
) ENGINE = InnoDB;

CREATE TABLE direcciones (
    idDireccion INT NOT NULL AUTO_INCREMENT,
    direccion VARCHAR(50) NOT NULL,
    cp VARCHAR(5) NOT NULL,
    ciudad VARCHAR(20) NOT NULL,
    provincia VARCHAR(20) NOT NULL,
    pais VARCHAR(20) NOT NULL,
    idUsuario INT NOT NULL,
    principal BOOLEAN DEFAULT false,
    activa BOOLEAN DEFAULT true,
    PRIMARY KEY (idDireccion),
    INDEX (idUsuario),
    FOREIGN KEY (idUsuario) REFERENCES usuarios(idUsuario)
) ENGINE = INNODB;

CREATE TABLE tiendas (
    idTienda INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    PRIMARY KEY (idTienda)
) ENGINE = INNODB;

CREATE TABLE sucursales (
    idSucursal INT NOT NULL AUTO_INCREMENT,
    idTienda INT NOT NULL,
    telefono VARCHAR(10) NOT NULL,
    direccion VARCHAR(200) NOT NULL,
    cp VARCHAR(5) NOT NULL,
    PRIMARY KEY (idSucursal)
) ENGINE = INNODB;

CREATE TABLE productos (
    idProducto INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    descripcion TEXT NOT NULL,
    precio DECIMAL(8,2) NOT NULL,
    idSucursal INT NOT NULL,
    PRIMARY KEY (idProducto),
    INDEX (idSucursal),
    FOREIGN KEY (idSucursal) REFERENCES sucursales(idSucursal)
) ENGINE = INNODB;

CREATE TABLE shoppers (
    idShopper INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(20) NOT NULL,
    apellidos VARCHAR(20) NOT NULL,
    PRIMARY KEY (idShopper)
) ENGINE = INNODB;

CREATE TABLE franjasHorarias (
    idFranjaHoraria TINYINT NOT NULL AUTO_INCREMENT,
    franjaHoraria VARCHAR(15) NOT NULL,
    PRIMARY KEY (idFranjaHoraria)
) ENGINE = INNODB;

CREATE TABLE pedidos (
    idPedido INT NOT NULL AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idShopper INT NOT NULL,
    idDireccion INT NOT NULL,
    fechaEntrega DATE NOT NULL,
    fechaCompra TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    idFranjaHoraria TINYINT NOT NULL,
    total DECIMAL(8,2) NOT NULL,
    PRIMARY KEY (idPedido),
    INDEX (idUsuario),
    INDEX (idShopper),
    INDEX (idFranjaHoraria),
    FOREIGN KEY (idUsuario) REFERENCES usuarios(idUsuario),
    FOREIGN KEY (idShopper) REFERENCES shoppers(idShopper),
    FOREIGN KEY (idDireccion) REFERENCES direcciones(idDireccion),
    FOREIGN KEY (idFranjaHoraria) REFERENCES franjasHorarias(idFranjaHoraria)
) ENGINE = INNODB;

CREATE TABLE pedidoProductos (
    idPedidoProducto INT NOT NULL AUTO_INCREMENT,
    idPedido INT NOT NULL,
    idProducto INT NOT NULL,
    unidades INT NOT NULL,
    precio DECIMAL(8,2),
    PRIMARY KEY (idPedidoProducto),
    INDEX (idPedido),
    INDEX (idProducto),
    FOREIGN KEY (idPedido) REFERENCES pedidos(idPedido),
    FOREIGN KEY (idProducto) REFERENCES productos(idProducto)
) ENGINE = INNODB;

DELIMITER //
CREATE PROCEDURE pedidosShopperSucursal 
(In shopper INT, sucursal INT, pedido INT) 
BEGIN 
    SELECT po.nombre, po.descripcion, pp.unidades, pp.precio FROM pedidoProductos pp, pedidos p, productos po WHERE p.idPedido = pp.idPedido AND pp.idProducto = po.idProducto AND p.idShopper = shopper AND po.idSucursal = sucursal AND p.idPedido = pedido; 
END //
DELIMITER ;
