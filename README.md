# Test Lola Market
## Instrucciones
- **Levantar Docker:** ejecutar "make" en el root del proyecto. *Se levantará en el puerto 8081 y en el 443, por lo cual se debe verificar que los puertos no estén en uso.*
- **Probar conexion a base de datos:** ejecutar "make ping-web-mysql" o puedes acceder https://localhost/phpmyadmin o http://localhost:8081/phpmyadmin que está instalado en el servidor web, usuario:lolamarket, contraseña:12345678.
- **Poblar base de datos:** ejecutar "make migrate-seed"
- **Query de Productos:** ejecutar "make rep-productos"

## Base de datos
Hay decisiones que he tomado por ser un test.

- En la tabla direcciones: pais, provincia y ciudad, deberían ser relaciones a tablas.
- Se podrían unificar las tablas de shopper y usuarios.
- Agregar un Estado al pedido con un historial de seguimiento.

El script para crear la base de datos se encuentra en data/db/creates.sql. El script para poblar la base de datos e encuentra en data/db/inserts.sql. Para poder ver el script al consultar productos que deben comprar los shopper en una tinda, debes ejecutar "make rep-productos" te pedirá los ID's que necesita, mostrará los datos y la query, además hay un ejemplo en /data/db/productos-tienda.sql.

## Docker
Dentro de la carpeta build/web he escrito un Dockerfile basado en Debian. Dentro de el está instalado apache y php7.4 para el funcionamiento de Symfony, además, al ser una maquina de desarrollo y facilitar el trabajo de los desarrolladores, tiene instalado phpMyAdmin.

Además del Dockerfile está toda la configuración que uso para la imagen de docker que se genera, configuraciones de apache, configuración de phpMyAdmin, certificados.

## Docker-compose
El archivo docker-compose.yaml se encuentra en deploy/development. Solo he usado dos máquinas, aunque se puede separar php de apache he preferido dejarlo así para tener un mayor acercamiento de lo que podría ser la máquina de producción. La base de datos está aparte y solo puede ser accedida por el contenedor web.

Los datos de mysql se generan en una carpeta llamada data/mysql que está mapeando al servidor mysql en la carpeta /var/lib/mysql y los archivos que creamos para generar la base de datos y poblarla estan en data/db mapeada al mismo servidor en la carpeta /root. En el caso del servidor web he mapeado la carpeta src/web a la carpeta /var/www/html del mismo.

## Makefile
En el makefile he creado varios "shortcuts" para facilitar la vida del desarrollador (todos deben ser ejcutados en el root del proyecto):

### make start
Inicia los contenedores

### make stop
Detiene y elimina los contenedores

### make logs
Accede a los logs de docker-compose

### make bash-web
Ejecuta bash en el contenedor web

### make mysql
Acceder al mysql dentro del contenedor

### make build-web:
Contruye la imagen del contenedor web en caso que se hayn hecho cambios

### make ping-web-mysql
Hace ping desde el contenedor web al de mysql (para comprobar la conexión de manera mas fácil)

### make migrate
Elimina la base de datos y vuelve a recrearla

### make seed
Inserta datos de Ejemplo

### make migrate-seed
Recrea nuevamente la base de datos y agrega datos de ejemplo

### make rep-productos
Imprime los productos de un pedido, el script pide los datos de shopper y tienda y pedido, y al final muestra los datos y la sentencia SQL ejecutada (La enunciado del ejercicio no pedia el numero de pedido, me ha parecido importante agregarlo en el caso que tenga mas pedidos asignados)

### make help
Ayuda

## .gitlab-ci.yaml
Comprobaciones al subir al repositorio de gitlab. A manera de prueba, se descargan las dependencias, luego se hacen pruebas de PSR2 y después se verifica que no haya errores de código en la carpeta src.

## Carpetas

### bin
Binarios utilizados para diferentes funciones. Algunos se pueden obviar y poner directamente en el Makefile pero por motivos de demo lo he dejado así.

### build
Archivos necesarios para contruir imagenes a medida de docker.

### deploy
Archivos para hacer un deploy de desarrollo y producción (En este caso solo desarrollo).

### data
Datos generados por el usuario o por los contenedores necesarios para su funcionamiento.

### src
Código de la aplicación.
